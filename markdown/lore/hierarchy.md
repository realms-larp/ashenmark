---
title: Heirarchy
...

:::{.links}

* ## Stories, Tales, and Legends
* [Mystery of the Emberwastes](./mystery-of-the-emberwastes.html)
* [The Golem Uprising](./the-golem-uprising.html)
* [The Mutates](./the-mutates.html)
* [The Imperium](./the-imperium.html)
* [Nangea](./nangea.html)
* ## Ashenmark Culture
* [Trials of Ashenmark](./trials-of-ashenmark.html)
* [Hierarchy](./hierarchy.html)
* [Theology](./theology.html)
* [Military Might](./military-might.html)
* [Regional Magic](./regional-magic.html)

:::

:::{.content}

# The High Seat

The High Seat of Ashenmark refers to the top echelon of Ashenmarks leadership. These three individuals form both the political and cultural heads of the nation.

## Highlord

The Highlord has the duties of leading the nation both in times of peace and war. It is the Highlord's responsibility to plan the work of the nation, assign tasks to subordinates, and see that the work is accomplished to the highest standard. Their duties also include recognizing it’s members, both through the ceremony of a new member swearing their sword to the nation, the but also the creation of new knights and lords. While the Highlord is the one responsible for anointing new knights, they are chosen by the knights and the Knight Commander. The Highlord is advised by the House of Lords, which includes the Knight Commander. The Highlord’s station is represented by the red flame of the nations heraldry.

## Knight Commander

The Commander of Ashenmark is the right hand to the Highlord. The Commander speaks with the voice of the Highlord in all matters and is senior to all other members of the nation. They are responsible for organizing the knights as well as seeing to a variety of responsibilities both domestically and internationally during conflicts. When the Highlord is not present the Commander assumes all responsibilities of his role in the group and is expected to be able to assume these duties at any moment. The Knight Commander bears the black shield of the heraldry of Ashenmark.

## Champion of Ashenmark

The title of champion is granted to a Sworn Sword in a competition that happens at various intervals. A champions tournament can be called at any time by the Highlord to allow for members to compete for the honorific title. The nations champion, while having no official duties, is often seen as representing the soul and might of the nation, and is tasked with inspiring all members of the nations by winning feats and glory in the name of the nation. They bear the emblem of the golden sword from the nations heraldry.

# House of Lords

## Knight of Ashemark

A title bestowed upon a Sworn Sword of Ashenmark who has demonstrated great commitment and performed great deeds in the service of Ashenmark. Knights are those who have accomplished great tasks or served for long periods of time. They are often experts in a particular field and highly competent in most others. They were created with the idea of being to Ashenmark what the Knights of the Eternal Flame are to the Realms at large. Knights are given a plot of land to settle and rule over. All knights also have a seat in the House of Lords.

## Lords and Ladies

A title given to senior members of the nation with a wealth of experience in a variety of skills, often leadership and governing. They are assigned to larger settlements where those skills are required to govern. The populace of those settlements tend to grow to resemble the ideals and practices of their leaders.

# Commonfolk

## Sworn Sword

A Sworn Sword of Ashenmark is a full member who has taken an oath to protect the land and its people. They do not swear an oath to an individual, but instead to the station of the Highlord, nation and people of Ashenmark. Sworn Swords also each take on an Initiate to mentor, and although they are tasked with the training of all Initiates within the nation, the one they are assigned to is their personal responsibility. 

## Initiate

An Initiate is the rank of petitioners of the nation. They are tasked with training in combat as well as completing the challenges of each of the Sworn Swords in the nation, as well as devising a challenge of their own to one date to be given to the Initiates that follow them.

## Vassals

Vassals are those folks born into, or settled in Ashenmark who have not taken the trials to become a Sworn Sword. Many are simply common people simply trying to carve out a peaceful life within the nation, while others, like the infamous Canary Cowl, are heroic figures who have chosen to eschew the established order created by the nation.

:::