---
title: Regional Magic
...

:::{.links}

* ## Stories, Tales, and Legends
* [Mystery of the Emberwastes](./mystery-of-the-emberwastes.html)
* [The Golem Uprising](./the-golem-uprising.html)
* [The Mutates](./the-mutates.html)
* [The Imperium](./the-imperium.html)
* [Nangea](./nangea.html)
* ## Ashenmark Culture
* [Trials of Ashenmark](./trials-of-ashenmark.html)
* [Hierarchy](./hierarchy.html)
* [Theology](./theology.html)
* [Military Might](./military-might.html)
* [Regional Magic](./regional-magic.html)

:::

:::{.content}

# Too lazy to add formatting tonight

The Path of Life & Nature

Blessing of Gaia (Life & Nature 1)
Uses: 5   Active: Meditate for 30 seconds, no weapons within 10 ft, kneeling or sitting
Once activated, caster may cast Heal Limb while moving and ignore weapon sensitivity while casting Raise Dead, until the next time the caster dies

Wildspeak (Life & Nature 1)
Uses: Unlimited
You may cast the Speak spell an unlimited number of times, but it may only be used on beasts or sentient plants.

Stoneward (Life & Nature 2)
Uses: 3   Active: Touch target for 15 seconds  Verbal: An explanation of the spell
When the target is struck by a normal weapon blow they may call Protection and ignore the hit.

Wisdom of the Bear (Life & Nature 2)
Uses: 3   Material: A tasty snack  Active: Offer the target your material component
You may offer a living creature a tasty snack.  If they accept your offer they will become non-hostile for a moment long enough for you to ask them a question.  If they are unable to answer the question, you may clarify the answer with the magic marshall.

White Rose Rejuvenation (Life & Nature 3)
Uses: 10   Material: An acorn, pinecone, or seed.  Verbal: An explanation of the spell
When cast on a dead body, this spell grants the recipient a basic regeneration. The spellcaster must hand the MC to the recipient when the spell is cast.

Amonita’s Cycle (Life & Nature 4)
Uses: 1   Verbal: Three words
Grants you a single cast the spell Combat Raise Dead.  Reset the use of this spell when you are raised to life from being dead.  This does not work if you are animated or otherwise raised as undead.

Hide of Oso (Life & Nature 5)
Uses: Unlimited
Whenever you raise someone from the dead, you also repair their armor.

Beast Form (Life & Nature 5)
Uses: 2    Material: A mask of a beast, may be provided by EH/MM   Active: Put on mask
Allows the caster to wield any florentine combination as if it were in their spell restriction.  They cannot cast spells outside of the Life and Nature ritual path while the spell is active.  While in this form their weapons are considered to be their hands, and thus cannot be put down or searched, and are not considered weapons for the purpose of other spells.  The spell ends when they take off the mask or when they have died three times after it has been activated.


Barkskin (Life & Nature 6)
Uses: Unlimited
You gain skin as tough as bark and may call 1 point sectional armor.  This armor can be repaired by casting the heal limb spell on a location of damaged armor.  The armor is also repaired on the respective limb when a limb is healed, and on all locations when you are raised to life (including regeneration).  This does not work if you are animated or otherwise raised as undead.


The Path of Death & Destruction

Destroy (Death & Destruction 1)
Uses: 1   Active: Touch an object with your hand  Verbal: 10 words, ending with Shatter!
You shatter the object you touch.  The object cannot be being wielded or worn by a living creature.

Dark Extraction (Death & Destruction 2)
Uses: 2  Verbal: 10 words
When used on a dead target in the presence of the Magic Marshal gives the caster a vision in the form of a flash of the creature's past.

Persist (Death & Destruction 3)
Uses: 3     Active: Meditate for 30 seconds, kneeling or sitting
The next time you die, you regenerate back to life in 120 seconds.  If you are raised, animated, or rendered soulless before you regenerate the use of the spell is wasted.  Once regenerated you cannot call armor or cast spells until you land a killing blow on another creature.

Soulbond (Death & Destruction 4)
Uses: Unlimited, one at a time   Active: Touch a target player character  Verbal: 10 words
When you touch the target you bond some of your soul to them.  Whenever the caster dies  and their player can see the target, after 30 seconds your corpse will get up and walk in the most direct (but OOC safe) path to the target as if under the effects of the spell Zombie Walk. If the corpse is interrupted, it will fall to the ground and the spell ends.

Primal Rampage (Death & Destruction 4)
Uses: 2     Active: Touch a willing characters  Verbal: An explanation followed by 20 words
Upon completion of the spell the target must loudly count to ten.  During this time they are unaffected by all weapon blows or spells as long as they are engaged in combat.  At the end of the 10 seconds, they die.

Mulch (Death & Destruction 5)
Uses: 3     Active: Render a creature soulless entirely on your own (does not work on undead).
Restore the uses of one of your spells, 3rd circle or lower.

Deathbringer (Death & Destruction 6)
Uses: 5   Active: Touch a weapon  Verbal: 10 words
The spellcaster enchants the target weapon, allowing it to swing Piercing on its next swing. Reset the uses of this spell when the caster renders a creature soulless full on their own.  If the weapons wielder fails to hit the target, or their attack is blocked or parried, the caster may recast this spell on their weapon without using up a casting.  You cannot reset the spell this way more than once per hour.

The Path of Balance

Death Knight’s Will (Balance 1)
Uses: Passive
If at any time the caster would be made undead or animated in any way they are able to retain their free will throughout the duration of the spell.  If you are rendered soulless you may still be animated as undead.

Sacrifice (Balance 2)
Uses: 5     Active: Touch the target      Verbal: 3 words
The target is raised from the dead, however the caster dies.  This cannot be prevented in any way.

Lay Low the Tyrants
Sacrifice (Balance 3)
Uses: Unlimited
You may call Piercing against other creatures who are wielding longer weapons than you.  You only may call this once per creature, hit or miss.

Might of the Crossroads (Balance 4)
Uses: 3     Verbal: 10 words
The caster may use weapons one restriction higher than they would normally be able to until the next time they die.

Fungal Adaptation  (Balance 4)
Uses: 1
The spellcaster is protected from any damaging attack for 1 hit. The call for this is “Resist Death.” The spellcaster can choose when to utilize this effect. Reset the use of this spell when you are raised to life or animated from being dead.

Redistribute (Balance 5)
Uses: 1     Active: Touch another characters spellbook  Verbal: An explanation of the spell
The target spellcaster may restore all uses of a single spell, fourth circle or lower.

Make Mighty the Meek (Balance 6)
Uses: 1     Active: Touch each target one by one while reciting the verbal     Verbal: 30 words
When you cast the spell you may target up to 10 other characters, naming them in the verbal.  At any point in the future when you die, you may call out, naming characters one followed by “Rise and Fight!”  If they hear you they are raised from the dead and their armor is repaired if they have any.  Once this is used successfully on a target, they cannot be raised by the caster in this way again.

The Path of the Elements

Windwall (Elements 3)
Uses: 5       Material: 30’ of white rope       Verbal: 20 words
The caster may lay the rope with the ends crossing forming a circle.  Those inside the circle are immune to the damage or effects of any projectile from outside the circle.

:::