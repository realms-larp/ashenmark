---
title: Theology
...

:::{.links}

* ## Stories, Tales, and Legends
* [Mystery of the Emberwastes](./mystery-of-the-emberwastes.html)
* [The Golem Uprising](./the-golem-uprising.html)
* [The Mutates](./the-mutates.html)
* [The Imperium](./the-imperium.html)
* [Nangea](./nangea.html)
* ## Ashenmark Culture
* [Trials of Ashenmark](./trials-of-ashenmark.html)
* [Hierarchy](./hierarchy.html)
* [Theology](./theology.html)
* [Military Might](./military-might.html)
* [Regional Magic](./regional-magic.html)

:::

:::{.content}

# The Pantheon of Ashenmark

The Pantheon of Ashenmark consists of a collective of gods who have folk that keep their faith within the nation. In the early days, Gaia was the most prevalent, whoever the other gods began to gain influence over time. The major theme of the Pantheon of Ashenmark is balance, and is sometimes referred to as the Pantheon of Balance. Many of the gods who find followers in Ashenmark are also worshipped in other places around the Realms.

## Gaia

Gaia is the oldest god who has power within the nation's borders, harkening back to a time before Ashenmark when the lands were claimed for Elemirre. Across the woodlands you can find small shrines to the goddess. These shrines are built around an individual tree and are recognizable by the rare vine that grows up the tree. This vine produces a set number of leaves which have powerful medicinal properties. These vines cannot grow the leaves back and once all the leaves are plucked from the vine, the vine and tree will both wither away.
Recently Lady Twenaria led the heroes of the Realms in a ritual to restore an ancient temple to Gaia. For their efforts Gaia blessed the forests in Ashenmark, keeping a Bedlam incursion at bay.

## Dionin

The worship of Dionin, the god of death and destruction first came into prevalence a few years after the Bedlam Wars. The first notable follower was the leader of Ashenmark at the time, Highlord Temorse Sorrowind. He was once a follower of the small god Autumn Rose, but after years of subservience to the mad titan, his connection to nature was severed, choosing instead to follow the ways of a deity more familiar to his experiences.

## Garm

Garm was the second god to take power in Ashenmark. When Killian Battlefate, Stripes Lightstride, and Raynor Skyline were taken as squires to the Knights of Garm a temple was contructed at the crossroads of Harts Hollow. The Ashenmark squires severed as liaisons when the heroes of the Realms attempted to restore the power of the Gods after the Bedlam Wars, however the heroes were deceived and Asmoedeous pinned Garm to his Throne with the Sword of Kings. Shortly after undoing this, Sir Killian was made Knight Commander of the Knights of Garm.

## Small Gods

### Autumn Rose

Autumn Rose is the 'oldest' of the small gods, having been around for three ascensions (she was created from the first one). Her symbol changed when her favored high priestess died, but then a young man carried the torch of worship, spreading it into the lands of Nadina, near-by to the Barony of Banecroft. Autumn Rose's temperament is that of a sad yet strong person, laughing through sadness, crying tears of joy. She has visited the Realms in the flesh numerous times, each time as a buxom strawberry-blonde, dressed in the fair of autumn strolling and a jaunty cap. She understands the cycle of things (as autumn is her season) and thus things must sleep or pass away to be made new again in Spring. She does not like necrotic magics nor the undead (viewing these as unnatural). Worshipers of Autumn Rose tend to lean towards Healing as a magical or mundane preference, and little shrines in the past few years have actually been showing up in odd places around the Realms.

The faith in Autumn Rose followed Temorse when he left the Banecroft to found Ashenmark. White rose gardens became commonplace for a time, however after Temorse died during the Bedlam War many of those gardens were destroyed. As part of the quest to restore Temorse Autumn Rose gave up much of her connection to Temorse, imbued with some of her divine power. Since then Autumn Rose has taken her place within the Ashenmark pantheon, or the Pantheon of Balance.

### Oso

Oso is less of a god, but a bear given the powers of a god. Met at Camp Despair, Oso was convinced to return to Ashenmark. Oso appears often as a normal brown bear, but is known for his great wisdom. He is associated with the simple things in life, especially sleeping and eating, his two favorite activities. While he is not very nuanced as far as divine beings go, he has been known to grant boons to those who bring him gifts of delicious food.

### Amanita

:::