---
title: Military Might
...

:::{.links}

* ## Stories, Tales, and Legends
* [Mystery of the Emberwastes](./mystery-of-the-emberwastes.html)
* [The Golem Uprising](./the-golem-uprising.html)
* [The Mutates](./the-mutates.html)
* [The Imperium](./the-imperium.html)
* [Nangea](./nangea.html)
* ## Ashenmark Culture
* [Trials of Ashenmark](./trials-of-ashenmark.html)
* [Hierarchy](./hierarchy.html)
* [Theology](./theology.html)
* [Military Might](./military-might.html)
* [Regional Magic](./regional-magic.html)

:::

:::{.content}

The armies of Ashenmark have become highly diverse since its inception.  Because Ashenmark has vast spaces of wilderness its population tends to be smaller, thus leading to alternate forms of armed forces being developed.

# Infantry

## Stone Legion

An ancient ruin was discovered near the town of Turnstone that contained arcane golems.  The ruin was massive and was buried deep underground, but after several ventures a large number of golems were able to be recovered and retrofitted into a fighting force.  Highlord Temorse sought to increase their numbers and effectiveness through converting the ruin into the Stoneheart, a high security arcane research facility working to improve the quality of the golems and potentially manufacture more.  This lead to the creation of the sentient golems, followed by the golem uprising.  Once the uprising was quelled the numbers of the Stone Legion were decimated and it was decreed that the research at the Stoneheart was to end and no more sentient golems were to be created.  This meant that the only way to increase the Stone Legions numbers was once again delving deeper into the ruins beneath the Stoneheart.

The Stone Legion is now made up of non-sentient golems powered by arcane magic.  They are incredibly durable and need no food, nor sleep.  While their programming gives them the ability to fight, they become predictable and have the skill of a well trained militia fighter.

## Ranger Corps

The ranger corps are one of the more elite fighting forces in Ashenmark with a focus on scouting and skirmishing rather than waging direct warfare.  Rangers are trained in bow, blade, and usually some light healing magic.  They are masters of traversing difficult terrain, having training that has taken them through forests, mountains, and swamps.  They are trained to move unseen and are generally used as scouts or ambushes in environments where they have plenty of cover.

## Battlemages

Battlemages have been trained for years at Goldenhall, specifically in the use of lightning magic.  They are strong offensive casters that can wreak untold devastation in large numbers, especially against armored foes.  The most elite of the group of battlemages have earned the title of the Scorpions of the West.

# Support Forces

## Gaian Healers

While many rangers are often embedded into regular forces to act as battle medics, the Gaian Healers are highly skilled and can rescue soldiers from even the most mortal of wounds.  They used a nature based magic and each receives individual training from Dame Twenaria.

## Warhounds

Trained exclusively at New Battlefate Keep by Killian Battlefate himself, Ashenmark has recently started using both combat oriented warhounds as well as tracking oriented scent hounds as a part of its military forces.  The warhounds are a larger breed, strong enough to wear armored barding, while the scent hounds are used as aid for ranger forces both as trackers and sentries.

# Calvary

## Heavy Horse

Ashenmark has access to few horses and fewer war horses.  Those few horses are trained to become war hoses for the nation's small armored cavalry corps.  Deployed generally with the main army, the heavy horse are used to break defensive infantry positions.

## Avian Outriders

A large flightless bird from the eastern forests has been tamed and are used as outriders.  They are very fast at sprinting over short open distances, which makes them excellent at targeting enemy archer battalions or support casters.  They really shine on battlefronts in heavily forested areas, moving much more nimbly through the underbrush compared to most horses.

# Naval Forces

## Gilded Fleet

Ashenmark has a small naval force stationed at Farport in Elemirre Isle.  This consists of several galleys, three caravels, and a single brigantine that acts as flagship, the Burlap Terror.

:::