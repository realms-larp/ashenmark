---
title: Trials of Ashenmark
...

:::{.links}

* ## Stories, Tales, and Legends
* [Mystery of the Emberwastes](./mystery-of-the-emberwastes.html)
* [The Golem Uprising](./the-golem-uprising.html)
* [The Mutates](./the-mutates.html)
* [The Imperium](./the-imperium.html)
* [Nangea](./nangea.html)
* ## Ashenmark Culture
* [Trials of Ashenmark](./trials-of-ashenmark.html)
* [Hierarchy](./hierarchy.html)
* [Theology](./theology.html)
* [Military Might](./military-might.html)
* [Regional Magic](./regional-magic.html)

:::

:::{.content}

# Petitioning Ashenmark

Those interested in petitioning Ashenmark are encouraged to seek and and talk to
members of the nation to first see if they feel like it is a good fit for them.
The next step is to seek out one of the members of the High Seat and request an
initial tribunal where they will be interviewed by the group. Once complete,
they may begin down their road to prove they are qualified to become a Sworn
Sword of Ashenmark.

# Trials

Each initiate must complete at least 5 of the following trials, in addition to that of the current leader of Ashenmarks trial. You may not begin the final challenge until you have completed the other five trials. Once all the trials are complete you may ask for your tribunal when you think you are ready to take the step into being a sworn sword.

## Temorse’s Trial

My trial is that of the leader. You will be assigned a special task and will be allowed to take a few select allies with you of your choosing from outside of Ashenmark. Once you have completed 5 other trials see me for your final one.

## Grindin’s Trial

My trial is that of the athlete. Prove that you have  the physical strength and endurance capable to fight as a unit. It entails running one mile and 20 pushups. Extra tests are at my discretion.

## Osric’s Trial

My trial is that of the collector. The initiate must collect 3 coins and  describe their history. The coins must be of 3 different materials, of 3 different values, from 3 different nations, at least 1 nation of which still exists and 1 nation which doesn't.
Excluded coins: rowans, coin of the realms, leviathans.

## Killian’s Trial

My trial is that of the swordsman. Every initiate must fight every sworn sword with a single short, florentine, sword and shield, hand and a half, and their best combo in armor. For spell casters- they get their restriction and a set of spells I will give them.

## Kaelkatar’s Trial

My trial is that of the loremaster. You must make an honest effort to aid in the exploration of our lands, and of the chronicling of our history.

## Raynor’s Trial

My challenge is that of the serviceman. You must attend 5 events where you help set up, take down and staff or NPC the event.

## Caleal’s Trial

My challenge is that of the historian. You must carve your story into the pages of Ashenmark’s history. Write a 2-3 page (single space) character biography. Include your past, present, and future goals, and any significant feats you have accomplished. Tell your story as only you can do, from your perspective.

## Umbra’s Trial

My trial is that of the traveled warrior. You must fight seven individuals from different nations outside of Ashenmark with a single short, florentine, sword and shield, hand and a half, and their best combo. Spellcasters may choose to fight limited or unlimited.

## Shader’s Trial

## Twenaria’s Trial

## Malakai’s Trial

## Kwido’s Trial

My trial is that of the community steward. You must submit a proposal to the Players Meeting, attend the meeting, and have it discussed there.

## Luc-Jean’s Trial

# Trials of Former Members

## Keela’s Trial

My trial is that of the warrior. You must attend 12 practices outside events.

## Dagan’s Trial

My trial is that of the scholar  A written test by me on basic rules of combat, and the spell system, along with a section of The history of Ashenmark. 

## Kahlenar’s Trial

My trial is that of the blacksmith. You must learn to create a light, durable weapon that is the envy of other nations. We pride ourselves on fighting, we should pride ourselves on the quality of our weapons. 

:::