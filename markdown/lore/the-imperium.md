---
title: The Imperium
...

:::{.links}

* ## Stories, Tales, and Legends
* [Mystery of the Emberwastes](./mystery-of-the-emberwastes.html)
* [The Golem Uprising](./the-golem-uprising.html)
* [The Mutates](./the-mutates.html)
* [The Imperium](./the-imperium.html)
* [Nangea](./nangea.html)
* ## Ashenmark Culture
* [Trials of Ashenmark](./trials-of-ashenmark.html)
* [Hierarchy](./hierarchy.html)
* [Theology](./theology.html)
* [Military Might](./military-might.html)
* [Regional Magic](./regional-magic.html)

:::

:::{.content}

* Awakened by Mayor
* Canary Cowls Role
* The Noxari Council, Galbraith
* The Factions
* The Old Pantheon
* Skyros vs the Queen
* Binding Pacifica
* The Betrayal of Anthur
* Plant Zombies
* Heist to Steal Artifacts
* Defeat of Anthur

:::