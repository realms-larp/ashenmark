---
title: Geography
...

:::{.links}

* ## The Central Province
* [Goldenhall](./geography.html#goldenhall)
* [New Battlefate Keep](./geography.html#new-battlefate-keep)
* [Hart's Hollow](./geography.html#harts-hollow)
* [Highmarket](./geography.html#highmarket)
* [Turnstone](./geography.html#turnstone)
* [The Stoneheart](./geography.html#the-stoneheart)
* [Elemirre Isle](./geography.html#elemirre-isle)
* [Farport](./geography.html#farport)
* ## The Western Province
* [Lucis](./geography.html#lucis)
* [Skyline](./geography.html#skyline)
* [Westwatch](./geography.html#westwatch)
* [Fort Stormguard](./geography.html#fort-stormguard)
* ## The Eastern Province
* [Faithhaven](./geography.html#faithhaven)

:::

:::{.content}

# Geography

## The Central Province

The original land, formerly the nation of Elemirre, gifted to the nation of
Ashenmark upon its formation. Its ancient forests remain wild and untamed for
the most part, and this is where visitors will feel the strongest connection to
Gaia.

### Goldenhall

Regent: [Sir Kaelkatar Anderlys](people.html#kaelkatar)

Long before Ashenmark laid claims to these lands, the village of Goldenhall
stood for many generations. Historically a series of large thatched roofed
longhouses stood in the center of town and with little space between them held
the appearance of one enormously long structure. The building sports a bright
golden roof that can be seen from almost anywhere in the city, and houses a
large tavern, many merchant stalls, and a performance amphitheater.

Annually on the Saturday following the second new moon after the first flooding
of the Anderlys River, the Festival of the New Year is celebrated at the great
hall in Goldenhall. Nobles and commoners alike from all over Ashenmark travel to
the great hall for a weekend of merriment, games of skill, and of trying to
foist your most unwanted possessions on others.

While Goldenhall contains no churches or temples, the city’s northern and
southern arterial gates are dedicated to the major gods of Ashenmark. The Gate
of Garm sits on the south of the city and leads to the industrial cities of
Turnstone and Highmarket while the Gate of Gaia leads northeast to the wooded
paradise of Hart’s Hollow. The gates themselves are flanked by masterful statues
of their respective deities whose stern gazes hope to deter any invaders. On the
western side of the city is the smaller Westward Gate which leads to the Bridge
of Triumphs that crosses the Anderlys and leads out to some of the newer
Ashenmark cities. The old bridge spanning the river was torn down shortly after
the victory over bedlam and the return of Temorse. Representing our conquering
of the darkness, the bridge is always brightly lit by dozens of magical torches
and during the spring and summer a handful of small gardens of white roses are
tended on its sides.

Built into the southern wall of Goldenhall, two stone castles, Highlord’s Hall
and the Knights Tower tower on opposite sides of the Gate of Garm. The slightly
larger of the two is the Highlord’s Hall, and the smaller keep is the Knights
Tower where the Knights of Ashenmark occasionally meet to discuss their
occasional campaigns and where Knight commander Kaelkatar resides.

Not too far from the Highlord’s Hall, a larger than life bronze statue depicts
Temorse victoriously holding the head of Forge aloft giving us a glimpse into
the final moments of the construct uprising of 1014. Savy historians or those
who were part of the campaign may note that Temorse himself was not actually
present at the battle where Forge was slain, but it is more a stern reminder as
to the outcome of the uprising. On a lighter note many also remark that it is
clear that both Temorse and Forge’s heads were cast from the same mold with the
highlord and the separatist commander both appearing comically similar.

### New Battlefate Keep

Regent: [Sir Killian Battlefate](people.html#killian)

New Battlefate Keep, named for former Champion of Ashenmark Killian Battlefate
is the training grounds for the Ashenmark military. Mostly consisting of
training fields in the woodland clearings for drills with the keep to the north.
The new keep itself is bare, being a fairly new construction, with mostly just
barracks for all the soldiers and quarters for Sir Killian who trains all the
new soldiers. This newer Keep was initially constructed to be border
fortification on the banks of the Anderlys, however with the previous Battlefate
Keep being apart of land gifted to Rhiassa, the fortification was expanded and
is now used as the primary military training grounds.

The keep also features a state of the art forge where many of the weapons of
Ashenmark are designed and crafted under Killians watchful eye. Ashenmarks
smaller population and relative army size relies heavily on having state of the
art weapons and armor, and ever since the pike controversy of North South War
1010, several checks of all weapons have been required to ensure they meet the
nations rigorous standards.

Since the new keeps location is closer to Hart’s Hollow, Twenaria’s influence
has grown in there. Sir Killian has incorporated several new combat units to the
ranks of the army, some inspired by Twenaria’s connection to nature, and others
by different members of the nation. While non-sentient golems serve as guards
and protectors of the settlements around the nation, there are no longer any
golem troops in the army following Forge’s Uprising in 1014. In addition to
training militia, and standard rangers, archers, and heavy cavalry, Killian has
sought fit to add a greater range of military might. First was adopting combat
sorcerers skilled in the use of lightning magic. This elite unit, nicknamed the
Scorpions of the West, seem to be inspired either by the great lightning
scorpion encountered during an excursion in 1013, or to the skill of such
lightning wielding sorcerers as Sir Kaelkatar or Sir Grindin. Aiding in
scouting, tracking, and flanking attacks were also the newly trained warhounds,
often accompanying rangers and light calvary on missions. Finally, the most
extravagant of the new additions, are unique large flightless bird found in the
forests of Ashenmark which a few elite animal handlers have been able to break
to use as mounts. These fleet footed avians are sure footed in the forest,
allowing the bird calvary to move quickly through densely forested terrain.

### Hart’s Hollow

Regent: [Dame Twenaria](people.html#twenaria)

Hart's Hollow itself is not much of a town. Closer to a village, really. Small
cottages cluster together here and there. This is a peaceful place, not one for
the hustle and bustle of a busy town. Small but bountiful gardens allow the folk
to trade among each other. To supplement, they fish and hunt only what they
need.

At the far end of the village area, sits a sentinel. Known for as long as memory
can recall as the Wistman, the giant tree spreads its boughs to the sky. The
surrounding rivers allow moss to cling to most surfaces. Rocks and trees alike
seem to sprout a pelt of green. Beyond, the Wistman's Wood slopes towards the
Grey Moor. These are wild and feral areas, not made for your easy stroll. It is
a place of savage beauty, and it thrums with life and death in full cycle. It is
in these woods that one can find a sacred glade of Gaia.

The Grey Moor is a wind swept land, with bare rocks and grasses clinging
desperately to the dirt. The grey rocks make odd formations that the locals
swear shift in the night. Small blots of light and odd creatures, half seen,
give the rumor mill more than enough to work with.

### Highmarket

Regent: [Sir Grindin Starbrook](people.html#grindin)

Highmarket was constructed at the confluence of three rivers, and the southern
banks of the Amber Lake. This location has lead to its natural status as the
center of trade throughout all of Ashenmark. Natural resources such as ironcane,
bloodvine, and umbral bloom are all highly prized throughout the Realms for
their alchemical properties as well as ironcane’s usage in creating the highest
quality weapons.

In addition to the three rivers that have traditionally been Highmarket’s
lifelines, there is a maelstrom in the center of Amber Lake. This maelstrom is
based on arcane technology that is derived from a similar portal in Nadina lake
in the Barony of Bancroft, the previous home of several founding members. Once
it had been conjured, there was a need to find a captain brave (or foolhardy)
enough to try it. Always one to face problems head on, Kahlenar steered his
vessel directly towards it. Kahlenar’s ship rode through the maelstrom unharmed,
reappearing out to sea, surprisingly close to where it had been planned. Thus
was created the maelstrom-pass, where yet another route of trade opened up to
the already busy merchants of Highmarket.

The wealth that has been brought about by Highmarket’s natural gifts have been
well utilized by Ashenmark’s industrious people. Highmarket represents the
promise of peace through prosperity. In keeping with his goals of providing ever
better lives for his people, Grindin worked closely with Kaelkatar and his
brother Garric to create a Library, where knowledge that flows through Ashenmark
can be made available to all. While he may have high ambitions, Grindin has
enough self awareness to know that governance is not exactly his strong point;
choosing instead to appoint specialists to advise and govern whilst he is on
campaign.

Highmarket also now features the Pacifican embassy within Ashenmark. Trade
between the Pacificans and Ashenmark has become common since the defeat of the
Imperium at the hands of the combined forces of King Skyros’ Pacifican forces
and the heroes of the Realms, and the Tidegate of Ashenmark helps facilitate
that trade.

### Turnstone

Regent: [Lord Osric Turnstone](#people.html#osric)

Turnstone is was the first town settled after the formation of the nation of
Ashenmark. It’s named after the first Champion of Ashenmark, who won the first
grueling Champion of Ashenmark Tournament at North-South War of 1010. It is
primarily the home to hunters, trappers, and those involved in the timber trade,
all of whom are watched very closely by the priest of Gaia at the temple outside
the town, to ensure they only take what they need, and that they avoid taking
from sacred places. Turnstones lumber yards product most the timber and ironcane
to supply the armies of Ashenmark. In town you can also find a house of balance,
a place where followers of Garm pay their respects.

The forested meadows nearby to Turnstone have also become common homes to
apiaries as honey bees were introduced to the area when it was settled. This has
led to an abundance of meadhouses in the area, including a local contest for the
brewing of mead that takes place annually, and is judged by the town's master
and namesake, Lord Osric Turnstone.

Turnstone was the target of attack from both fire spirits and the ancestral
ghosts of aspis magi who inhabited the nearby area. Their peace was disturbed
when the town was built close to the ruins now known as the Stoneheart and the

### The Stoneheart

Regent: The Wardens of Stonebrook

The Stoneheart was once an ancient aspis ruin. The aspis who lived there long
ago had created an arcane city using golems as soldiers and to perform manual
labor. Many years after their city collapsed, adventurers from the Realms
cleared out the ancient evils that remained in the ruins, uncovering a vast
workforce of golems. Mages from Ashenmark moved into the ruins to see if they
could repurpose the golems, to much success, even going as far as creating
sentient golems under the orders of Highlord Temorse. However, some of the
sentient golems were corrupted by the evil material, bloodstone, when trying to
gain autonomy. These events lead to the golem uprising by the golem Forge. While
the forces of Ashenmark engaged the Forges golem army, heroes of the Realms
dispatched his lieutenants and eventually forge himself. Several of the sentient
golems, notably Spark, were infused with nature magic in a ritual to Gaia,
granting them the autonomy they sought without the corruption of the bloodstone.

After the golem uprising the Stoneheart was cleared out and converted into a
high security prison, to be paid for by Ashenmark, but staffed by various
nations of the Realms, in order to keep the most powerful foes of the Realms
captive

### Elemirre Isle

Elemirre isle is a small island off the coast of Teng Hua acquired by Ashenmark
during the completion of the Tidegate. The island is largely uninhabited by
people, but has a diverse and populous wildlife, earning the name of Twenaria’s
original home. Access to the isle is totally dependent on the Tidegate, an
enchanted whirlpool that serves as a portal between Ashenmark proper and the
isle.

### Farport

Regent: Throne of Ashenmark

Farport serves both as a minor port as well as the home of Fort Swingline, a
defensive fortification that protects the harbor and the Tidegate. Due to the
port being outside the greater influence of Ashenmark proper, and the company
that its former Regent tended to keep, its streets are often overrun with bawdy
seadogs and drunken rapscallions. While merchant ships travel through the
Tidegate to access the trade houses of Highmarket, weary travelers looking for
rest, revelry and drink tend to frequent Farport, especially its massive tavern,
the Burlap Tankard, said to be able to fit the entire Realms within its walls.

## The Western Province

The lands west of the Anderlys river were incorporated into Ashenmark several
years after the nation was formed. The land was mainly plains and mountain,
deal for industry and farming so that Ashenmark's growing population could be
sustained by food grown in its own borders.

### Lucis

Regent: [Dame Umbra Lucis](people.html#umbra)

The agricultural hub of Ashenmark was given into Dame Umbra’s care upon her
knighthood. Given its fertile soil and favorable weather, it drew many people
who had a connection with the earth and the goddess Gaia. Through their
agricultural knowledge and religious dedication, it was no hard task for them to
tame the wilds that had been there before.

The people had built their settlement up from nothing and took great pride in
their accomplishment. Therefore, when Highlord Temorse came to them with new
declarations, they were not happy. Now recognized as a city, they would be fully
taxed as any other settlement in the nation. Furthermore, they would be governed
by a heathen who cared not for their most revered Gaia. Finally, they would even
be named after her. They had never decided on a name for themselves, but they
certainly didn’t identify as Lucians.

These people were farmers, not warriors or politicians. Therefore, as dissident
as they were, they knew not how to push back. And so, they carried on, waiting
for the spark that would inspire them.

With Dame Umbra away so often for matters abroad, she has assigned a steward to
govern in her place. Pryna, a young man from humble beginnings, seeks to
maintain the peace.

### Skyline

Regent: The Throne of Ashenmark

The town of Skyline, named for current Champion of Ashenmark Raynor Skyline, is
primarily a small mining town in north western Ashenmark. After the demonic
experiments were stopped at the neighboring fort Westwatch in 1014, the town has
been slow to revive with only Caleal’s silver mine making a small but consistent
profit. Many of the town’s miners went missing during that time and were
presumed dead or worse. Skyline now resembles a ghost town, with many run down
and empty homes and shops and a larger population in the cemetery than living in
the established town limits. However, those who remain are a hearty people who
have taken the restoration of the town upon themselves, and the town is
beginning to attract new citizens looking for hard work and a new life.

Skyline’s primary export is silver, but there are several established mines for
other base metals such as copper and tin. The iron mine was depleted in 1013 and
prospectors have not yet succeeded in finding new veins. There are rumors of
gold in the hills north of town on the border between Periden, but the deeds for
those mines have long been lost and the mines are abandoned. Few have gone on
expeditions there on their own, and fewer have returned. Those who have returned
speak of strange lights and whispers in the caves, and try to ward off other
would-be treasure seekers. Skyline is always open to accepting new refugees, but
are very wary of strange magics and regard anything relatively demonic with
contempt. The wounds from 1014 are still very fresh and signs of this can be
found everywhere. Should one come to visit or pass through, we recommend keeping
to the town center where they will find friendly yet tired faces, and that
travelers keep their artifacts and magics to themselves & well hidden. The
bakery Raisin Fight makes an excellent pecan tart and is a must-stop for anyone
with a sweet tooth.

### Westwatch

Regent: The Throne of Ashenmark

Westwatch was the first settlement created after Ashenmark obtained its western
land. A small motte and bailey fort it served as a safe haven while roads and
settlements were constructed to the west. It is the notable long-term home of
Stripes Lightstride.

### Fort Stormguard

Regent: Canary Cowl

Fort Stormguard, named for former nationals Dagan Stormrider and Keela
Loveguard, is a fortification built to garrison soldiers sent to western
Ashenmark to deal with bandits in the mountains. The western mountains are rich
in minerals, and thus mines and have been a constant target of various bandit
groups in the area. The fort and town were the location at which the town
magistrate first disturbed the Imperium wards, setting free the ancient empire
from its stasis.

For her part in liberating the town, and despite their differences, the bandit
leader known as Canary Cowl and her band of outlaws were granted a pardon as
well as command of the fort, seeing as she proved herself a competent leader and
had the respect of the villagers residing in the region. As a part of this
agreement, those villages have become exempt from a portion of the Highlord's
Tax.

## The Eastern Province

Formerly the lands of Ekembria, its ownership was negotiated by Highlord
Sorrowind along with their former master, Lord Dresh-ma Keth. The lands had
largely been abandoned and returned to nature, but had also hosted a wealth of
banditry who had been using the unclaimed lands as a safe point to conduct raids
on. The forests here are dense and the Ekembrians had but only begun to explore
the vast wilderness and its mysteries before their sudden disappearance.

### Faithhaven

Regent: The Throne of Ashenmark

Faithhaven was the only remaining settlement of Ekembria when Ashenmark took
over administration of the lands. It was a ghost town with few inhabitants, only
those too stubborn to leave had remained as others disappeared or moved on.
Those that did remain however have a strong lay of the land around Faithhaven,
and persist despite the abundance of bandits and gnolls in the region.

:::