---
title: Historical Timeline
...

:::{.links}

* [1010](#notes-from-1010)
* [1011](#notes-from-1011)
* [1012](#notes-from-1012)
* [1013](#notes-from-1013)
* [1014](#notes-from-1014)
* [Temorse's Sorrow(ind)](#stuff-temorse-hasnt-sorted-and-described)

:::

:::{.content}

# Timeline of Events in Ashenmark

## Notes from 1010

May 5th, 1010

: Founding of the nation under Highlord Temorse Sorrowind with Kahelnar
  Swingline, Grindin Starbrook, Killian Battlefate and Osric Turnstone. Former
  lands of Elemirre gifted by Dame Twenaria at the Feast of Chimeron. 

June 19th, 1010

: Tark Bloodragon accepted as initiate of Ashenmark at the North South War of
  1010. 

December 11th, 1010

: Squire Grindin knighted as Sir Grindin, Knight of the Sable Dragon by Knight
  Commander Sir Baron Diamond at the Yuletide Celebration at Cecil's Tavern. 

## Notes from 1011

April 17th, 1011

: The Red Tide raids several graveyards in Ashenmark to use its contents to
  create an undead army. All remaining dead are moved to guarded mausoleums. 

  Osric wins Champion's Tournament, becoming the first Champion of Ashenmark at
  the North South War of 1011. 

June 11th, 1011

: Kaelkatar Anderlys and Stripes Lightstride accepted as initiates of Ashenmark
  at the North South War of 1011. 

October 22th, 1011

: Kahlenar Swingling is made a Squire of the Knights of Garm at the Black and
  White Ball in Chimeron. 

November 19th, 1011

: Caleal Deiard and Raynor Skyline accepted as initiates of Ashenmark at the
  Order of the List Invitational Tournament of 1011. 

## Notes from 1012

January 14th, 1012

: Grindin Starbrook becomes an Apprentice to the Magi of the Realms under the
  direction of Sir Magus Malaki, Lord of Folkestone at the Feast of Leviathan. 

April 29th, 1012

: Kaelkatar Anderlys becomes an Apprentice to the Magi of the Realms under the
  direction of Dame Magus Phoenix of Folkestone at the Tournaments of the Blue
  Rose. 

May 13th, 1012

: Highlord Temorse was slain in Bedlam during the invasion of Chimeron. 

June 10th, 1012

: Lady Twenaria is named Lady of Hart’s Hollow the North South War of 1012. 

  Umbra Lucis, Dagan Stormrider, and Keela Loveguard accepted as initiates of
  Ashenmark at the North South War of 1012. 

  Kaelkatar Anderlys finishes his initiation and joins the ranks of the Sworn
  Swords at the North South War of 1012. 

  Osric wins the Champions Tournaments for the second time at the North South
  War of 1012.

September 15th, 1012

: Killian Battlefate is made Squire of the Knights of Garm during a quest at
  the Nexus. 

September 22nd, 1012

: Killian Battlefate, Kaelkatar Anderlys, and Raynor Skyline along with heroes
  of the Realms helped to fight an invasion of Ashenmark by Bedlam lead by a
  Bois incarnation of Highlord Temorse. Lady Twenaria, aided by the heroes of
  the Realms, called to Gaia for aide, restoring an ancient shrine. Much of the
  lands were taken by Bedlam, however all of the forested areas of Ashenmark
  were protected by Gaia. 

## Notes from 1013

January 12th, 1013

: Raynor Skyline finishes his initiation and joins the ranks of the Sworn
  Swords at the fifthteenth Feast of Leviathan.

  Caleal Dieard is made Squire of the Knights of Chaos during the fifthteenth
  Feast of Leviathan. 

June 22nd, 1013

: Caleal Dieard finishes his initiation and joins the ranks of the Sworn Swords
  at the North South War of 1013. 

June 23rd, 1013

: Killian Battlefate wins the Champions Tournament, taking the title from Osric
  and becoming the second champion of Ashenmark at the North South War of 1013. 

September 21st, 1013

: Members of Ashenmark along with Tao of Blackwood, Vesper of Blackwood, David
  of Blackwood, and Kaylan of Kalithnos meet with Autumn Rose to travel to the
  river Styx in order to gain access to a sliver of Temorse' soul. The quest
  was a success, but resulted in Autumn Rose fading from godhood. 

  Members of Ashenmark along with heroes of the Realms lead an attack on the
  Bedlam occupied city of Harts Hollow. Using a seal of Justari Balthazar of
  Eagles Rook performed a ritual to allow the heroes of the Realms to cleanse
  the city shrouded in darkness. 

September 28th, 1013

: Heroes of the Realms drive the Temorse-bois back through the maw of a Bedlam
  apparition and sunder it to pieces. Upon its destruction both the
  Temorse-bois and Temorse emerged from the wreckage. Temorse and all the
  present members of Ashenmark use Tale Unwritten to drive the lights blade
  through the Temorse-bois, destroying it for all time and restoring Temorse'
  soul. 

October 13th, 1013

: Kahlenar Swingling is named Commander of Ashenmark at Ashen Bounty. 

## Notes from 1014

January 11th, 1014

: Kaelkatar Anderlys was inducted into the Magi of the Realms at the sixteenth
  Feast of Leviathan. 

June 13th, 1014

: Members of Ashenmark, Blackwood, and Chimeron ventured into Hell to cleanse
  the demonic corruption from the souls of Killian Battlefate, Raynor Skyline,
  and Caleal Deiard. While the souls of Killian and Raynor were able to be
  cleansed, Khorne kept his hold on Caleal, even though Khorne was subdued for
  the time being. 

June 15h, 1014

: Dagan Stormrider finishes his initiation and joins the ranks of the Sworn 
 Swords at the North South War of 1014. 

  Keela Loveguard finishes her initiation and joins the ranks of the Sworn
  Swords at the North South War of 1014. 

  Umbra Lucis finishes her initiation and joins the ranks of the Sworn Swords
  at the North South War of 1014. 

  Kahalenar Swingline wins the Champions Tournament, taking the title from
  Killian and becoming the third champion of Ashenmark at the North South War
  of 1014. 

June 28th, 1014

: Dagan Stormrider was squired to Mestoph Darkling of the Knights of the Blue
  Rose at the Tournaments of the Blue Rose IX. 

  Keela Loveguard was squired to Aeston Stromgate of the Knights of the Blue
  Rose at the Tournaments of the Blue Rose IX. 

  Kwido Sedna was accepted as an initiate of Ashenmark and wins the Youngbloods
  tournament at the Tournaments of the Blue Rose IX.

## Stuff Temorse hasn't sorted and described

Kwido Knighted to Magi - Leviathan 2020\
Shader Knighted to Lunar Aegis - Huntress 2023\
Shader Knighted to KoEF - QoH 2023\
Temorse Knighted to KoR - Leviathan 2021\
Malaki joins Ashenmark - QoH 2023\
Killian and Raynor Knighted to KoGarm by Nighthawk - Black and White\
Killian made Knight Commander\
Raynor squired to KoSD to Grindin - 2019\
Kael Knighted to Magi - ???

6/4/17 - Dagan, Keela, Kahlenar resign their oaths of fealty

Team Umbra Competes at QoH\
Raynor wins Champion of Queen of Hearts

:::