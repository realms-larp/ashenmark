---
title: Knights of Ashenmark
...

:::{.links}

## The Knighthood
* [Tenets & Ideals](#tenets--ideals)
* [History](#history-of-the-knighthood)
## The Knights
* [Grindin](#grindin-starbrook)
* [Killian](#killian-battlefate)
* [Kaelkatar](#kaelkatar-anderlys)
* [Umbra](#umbra-lucis)
* [Raynor](#raynor-skyline)

:::

:::{.content}

# The Knighthood of Ashenmark

In Ashenmark, the title of Knight is bestowed upon those who have made 
consistent and great contributions to the nation, either in or out of character.
A knight is expected to have mastered at least one related discipline and be 
competent in most others, such as weapon or armor craft, fighting or spell 
casting, event throwing, leadership, and cooking. They are those within the 
nation who are able to and have in the past taken up the mantle of leadership 
when it is required of them. They understand the importance of the bonds of the 
people within the nation and seek to strengthen them. The oaths of the Knights 
of Ashenmark are very similar to those taken when they became a Sworn Sword, and 
they are in some sense a renewal of that same oath.

The knights are led by the Knight Commander of Ashenmark, currently
[Kaelkatar Anderlys](../ashenfolk/kaelkatar.html). The station of Knight
Commander is one that not only is responsible for the knighthood, but also for
advising the Highlord of Ashenmark in all affairs.

## Tenets & Ideals

Service

: A knight should seek to serve their nation and its people, as well as those of
  the greater Realms. Their worth should be measured with the sweat, blood, and
  tears expended in the service of this community.

Fidelity

: A knight should seek to perform their service with fidelity, to be faithful to
  their nation, and to be dependable. While a knight should know their limits,
  they should seek to always push those limits even further.

Passion

: A knight should also perform their service with passion. If not passion for
  the task at hand, passion for their nation and community. It is passion that
  keeps the flame alit in the heart of a knight and those they wish to inspire.

## History of the Knighthood

The Knights of Ashenmark were founded in 1015 at the Black and White Masquerade.
Highlord Temorse named Kahlenar Swingline, Killian Battlefate, and Grindin
Starbrook Knights Progenitor of the Knights of Ashenmark. The Knights Progenitor
are the first three Knights of Ashenmark and were initially created as a way for
those knights to pass on their titles as they take a step back. As such these
three individuals are being recognized for their achievements, and eventually
the idea of progenitor knights was discarded when only Kahlenar took a step back
from active engagement in the nation. Since then Kaelkatar Anderlys was
knighted, taking the title as first Knight Commander of Ashenmark, followed by
Umbra Lucis and Raynor Skyline being made knights.

# The Knights of Ashenmark

## Grindin Starbrook

Grindin was knighted at the Black & White Masquerade in 1015. Grindin, more than
any other member of the nation, was always a jack of all trades. Capable of a
wide variety of skills, Grindin was always a vital person to have on a quest.
Be it his skill as a healer, a swordsman, or an archer, or his breadth of
knowledge from fire building, to knot tying, or improvised construction, Grindin
could handle it.

Grindin had also previously been recognized as a Knight of the Sable Dragon,
given his skill as a teacher. He carried this skill set forward into the
founding of Ashenmark, becoming an important individual in training new
generations. Not only was he able to learn any skill quickly, he was also
capable at imparting that knowledge to future generations with similar speed and
depth.

## Killian Battlefate

Killian was also knighted at the Black & White Masquerade of 1015. Killian was
well-known as an extremely skilled fighter, having held the title of Champion of
Ashenmark previously. Killian, while well liked by much of the community, tended
to serve more quietly, having become known as an excellent individual to help
staff an event. He was also an extremely skilled craftsman, particularly with
weapon crafting, having won the Queen of Hearts Foamsmith tournament three
times. Killian had served as Champion for Queen of Hearts teams and competed
multiple times in the Order of the List Invitational.

Moreso, Killian had an incredible sense of duty. If there is an important task
to be done, Killian will always make sure that it gets done, and done well, even
if it comes at some personal cost. He is also willing to take a stand for what
he knows is right, not shying away from conflict when it is needed. He performs
all of his duties with great care and humility.

## Kaelkatar Anderlys

Kaelkatar was the first non-founding member of Ashenmark to obtain the title of
Knight. He was knighted in 1016 at the Ashen Bounty. Kaelkatar is a storied
questor who has mastered both combat and magic. Previous to his knighting, he
had been made Magus of the Realms. Kaelkatar was the first person to join
Ashenmark following its founding and was an immense contributor to the nation
and community from the get go. In Ashenmark, he was known for his heavy
involvement in the creation and writing of Ashen Bounty and following questing
events. He has a mind for systems and design which has led him to being involved
in most subsystems found at Ashenmark events as well as many changes to the
game.

Kaelkatar was also immensely important to the social fabric in early Ashenmark.
The nation's bonds, being widely spread out geographically on an out of game
level, were greatly strengthened by his efforts to ensure that everyone was
included and had the attention they needed to grow. Likewise, Kaelkatar was an
important figure in forming many of Ashenmarks international bonds, and being
the sole member of Ashenmark responsible for Blackwood's national Ashenmark Day,
for his selfless deeds in restoring a Blackwood town.

## Umbra Lucis

Umbra was knighted at Ashen Bounty in 1017. Umbra was part of the second
generation of people to join Ashenmark. She quickly became known as a skilled
fighter, especially with a hand-and-a-half and with a great weapon. She coupled
this fighting skill with a strong analytical sensibility and a natural ability
to lead. Her partnership with her future out-of-game husband, Raynor, helped to
bring out the best of him, as he did with her.

Umbra quickly took the reins as one of the most important members of the
event-throwing team, proving to be a workhorse. She proved herself as a strong
writer, an adept prop-maker with the ability to create props in many different
mediums, and even showed some rarely seen role-playing skills as she took on the
role of Canary Cowl. Umbra is never afraid to speak her mind, albeit often
bluntly, which has led to many changes and improvements to how the event team
organized and ran events. While she never worried about credit for any of these
changes, her deeds would affect players' experiences positively for years.

## Raynor Skyline

Raynor was knighted at Ashen Bounty in 1019. Raynor quickly became the heart and
soul in Ashenmark, known for his laid-back and carefree attitude. His strong
personality helped him make a mark on the Realms, and while he was often content
to be comical in nature, he can command respect quickly with a tonal shift.
Raynor was an excellent fighter, favoring the sword-and-shield style, and was
the first member of Ashenmark to compete for and win the title of Queen of
Hearts Champion, with his honor and valor being of particular note in the
competition. As mentioned with Umbra, their pairing helped to bring out the best
in both of them.

Raynor's true acclaim within Ashenmark came from the incredible ease in which he 
could slip into a role. Debuting this skill initially playing himself having
been taken hostage at an event, he would later bring to life the character of
Skyros, a major character who was a favorite of players throughout the Imperium
plotline and beyond. He has also played numerous other small roles that always
get a callout in reviews and often need to be rewritten to become a more major
character due to player interest. Raynor's charisma will forever be legendary
among the future generations of Ashenmark.

:::