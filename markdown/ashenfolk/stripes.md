---
title: Stripes Lightstride
...

:::{.links}

* ## The High Seat
* [Temorse - Highlord](./temorse.html)
* [Kaelkatar - Knight Commander](./kaelkatar.html)
* [Shader - Champion](./shader.html)
* ## House of Lords
* [Twenaria](./twenaria.html)
* [Grindin](./grindin.html)
* [Killian](./killian.html)
* [Osric](./osric.html)
* [Umbra](./umbra.html)
* [Raynor](./raynor.html)
* [Malaki](./malaki.html)
* ## Sworn Swords
* [Kwido](./kwido.html)
* [Caleal](./caleal.html)
* [Luke-Jean](./luke-jean.html)
* [Tark](./tark.html)
* [Stripes](./stripes.html)
* [Moriah](./moriah.html)
* [Lyanna](./lyanna.html)
* ## Initiates
* ## Vassals
* ## Historic Members
* [Kahlenar](./former-members.html#kahlenar-swingline)
* [Keela](./former-members.html#keela-loveguard)
* [Dagan](./former-members.html#dagan-stormrider)

:::

:::{.content}

# Stripes Lightstride

## Accolades and Titles

* Sworn Sword of Ashenmark
* Squire to the Knights of Garm

## Biography

Stripes Lightstride comes from a land across the sea. He lived there in the
forest of Arthan among a people very like elves. They lived long, simple lives
of tending to their forest home and communing with nature. They were mostly a
peaceful people, but once rallied they could be fierce warriors in the name of
Arthan. Stripes served as the druid guardian to his people’s deity, an ancient
tree spirit named Urbog. Upon becoming the guardian, he was assigned an animal
companion. It was a squirrel he came to know simply by Stripes. It was in honor
of his friend that he took the title of Stripes upon arriving in the Realms.

:::