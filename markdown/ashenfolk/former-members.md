---
title: Former Members
...

:::{.links}

* ## The High Seat
* [Temorse - Highlord](./temorse.html)
* [Kaelkatar - Knight Commander](./kaelkatar.html)
* [Shader - Champion](./shader.html)
* ## House of Lords
* [Twenaria](./twenaria.html)
* [Grindin](./grindin.html)
* [Killian](./killian.html)
* [Osric](./osric.html)
* [Umbra](./umbra.html)
* [Raynor](./raynor.html)
* [Malaki](./malaki.html)
* ## Sworn Swords
* [Kwido](./kwido.html)
* [Caleal](./caleal.html)
* [Luke-Jean](./luke-jean.html)
* [Tark](./tark.html)
* [Stripes](./stripes.html)
* [Moriah](./moriah.html)
* [Lyanna](./lyanna.html)
* ## Initiates
* ## Vassals
* ## Historic Members
* [Kahlenar](./former-members.html#kahlenar-swingline)
* [Keela](./former-members.html#keela-loveguard)
* [Dagan](./former-members.html#dagan-stormrider)

:::

:::{.content}

# Kahlenar Swingline

## Accolades and Titles

* Former Lord of Elemirre Isle

## Biography

Kahlenar came from the South Seas, many leagues from the land known as the
Realms. He grew up in the town of Port Royale, seat of power for the Crimson
Coast, the colonies of an empire far across the sea. His parents were merchant
sailors, and often one would be away, sometimes both, so with little supervision
Kahlenar caused his fair share of trouble, and got into some of it.

When he was sixteen his parents' merchant vessel was lost at sea. No one ever
found out what happened but there was much speculation. Some said storms had
taken her, others said krakens, but most said pirates. The wiser folk know this
is because storms are too boring and krakens are too rare. Within a month
Kahlenar got himself recruited to a small vessel looking for some cheap labor.

It wasn't more than a fortnight before this vessel was attacked by pirates.
They came upon the tiny vessel in the night and attacked by dawn. Kahlenar
himself grabbed a rapier and hid in the aft deck, crouching behind some crates
and netting. Each time an attacker was within arms reach Kahlenar stab them in
the back. By the end of the attack there were four dead behind the hiding place.
However, the pirates won out and the vessel was captured with all remaining crew
set to the lifeboats. The captain, however, noticed Kahlenar's body count,
praised his guile, and he and the pirate crew welcomed him aboard the Alcazador.

# Keela Loveguard

## Accolades and Titles

* Squire to the Knights of the Blue Rose

## Biography

Far to the east, lies the peaceful duchy of Hiedenkries. A place of beauty and
prosperity. Its people are a mixture of elves and humans living harmoniously
side by side. During the summer months the official business is trade, and with
a large deep harbor, the only major problem was dealing with the occasional
Corsican pirates who also lived by trading.

Hiedenkries was an autonomous duchy, a grand duchy, showing no allegiance to any
kingdom. The Duke, a natural leader and warrior had led their people through
difficult times and good. One day, the Duke and his wife, Duchess Margaret
Loveguard Macnamara welcomed a daughter into their world. They named her Keela.

As she grew, it became clear early on that Keela took after her warrior father,
Sean Aengus Macnamara. She took well to hand to hand combat, archery, and
horsemanship. She especially excelled in swordplay. The Duke, much to his wife’s
dismay, encouraged his daughter in martial arts and leadership on the
battlefield. He knew that one day she may have to fend for herself.

When the Duke was just a baby, his parents, the reigning Duke and Duchess, were
murdered in their sleep by assassins. Sean Aengus and his brother Bryan would
have also been killed, had they not been visiting their Aunt Brianna. This was a
very difficult time in the History of Hiedenkries. Lady Brianna became Duchess
as Sean Aengus and Bryan were too young to rule.

As time went on, Sir Bryan Macnamara and Duchess Brianna would both fall in
battle while pacifying the lands around Hiedenkries, that ensured the long
stretch of peace that they now currently enjoyed. The title of Duke passed to
Sean Aengus and for a long time, there was no sign of the assassins and none
were ever caught.

Years later, the assassins returned. This time Keela’s mother, the Lady Margaret
Loveguard was their target. However, the would be assassins, kidnapped Lady
Margaret and made off with her. These assassins have made attempts on the noble
family for generations, but the secret as to why they wanted us dead was lost
when Duchess Brianna died.

My father, the Duke, then did something unprecedented. He decided our family
would be better protected by moving us to Folkestone. He made Lord Dalamar, the
High Mage at the time, Regent of Hiedenkries. He chose Folkestone because of a
long standing alliance that began when both nations defended the ancient
northern castle Gillman, during the famous castle sieges of Rrathchlwynn.

During her years in Folkestone, Lady Keela proved herself in battle many times
and became a renowned warrior in her own right. She joined the ranks of many
other female warriors in Folkestone such as Katasha, Kara, Miranda, Pheonix and
Saphire, helping to carry the day by bringing death and defeat to their enemies.
Training under Lord Jarrod, Collin, Sean Aengus and others, she honored her
warrior skills for a special quest that would change her life, a quest to find
her mother. Keela knew she was ready to undertake the next step in her venture.

The only clue she had to find the assassins, and possibly her mother, lay in the
fact that the assassins fled south. So, taking her father’s leave, and with a
teary farewell to her friends in Folkestone, she set out on her journey south.

After months of wondering, Keela looked up two friends who supported her when
she ran for Queen of Hearts years earlier. Her general, Lord Temorse, and
Kahlenar The Dread Pirate came from a Southern land called Ashenmark. When she
arrived, she was greeted warmly by the people she met. Keela realized she had
found a new home in the South, surrounded by new friends. She later traveled
with Ashenmark to Folkestone in the summer of 1012 where she was officially
sworn into Ashenmark before her friends, new and old, and her proud father.

Killian Battlefate, Sworn Sword of Ashenmark became Keela’s official Mentor. She
immediately embraced the tasks and challenges he presented to her. Some of these
included physical fitness, combat competency and forging new weapons.

Lady Keela, happy in her new home in Ashenmark, continues her quest which now
includes supporting her new friends, saving Lord Temorse from Bedlam, finding
her mother and settling a score with a group of assassins who have plagued her
family for generations.

# Dagan Stormrider

## Accolades and Titles

* Squire to the Knights of the Blue Rose

## Biography

Deep within the northern lands of the Realms lies the small town Coal Town, of
Tuath Fasach. Coal Town is a newly formed and poor town. It was built around a
large coal vein that gave it it's name. The town is the host to many farmers,
merchants and traveling entrepreneurs of all sorts. Coal Town had only one
armorsmith, Darius Stormryder, who would travel to all lands of the Realms
intending to sell his ware. It wasn't long after the establishing of Coal Town
that Darius, and his wife Lynessa would conceive their first and only child. A
son, which they had named Dagan.

As a young boy, Dagan was never able to mask his desire for adventure, and a
life more thrilling than that in which was offered at home. Dagan's father was
always gone for weeks, sometimes months at a time due to his craft. During the
times his father was away, Dagan would often pretend he was on his own
adventure, his own personal quest. His imagination would always lead the quiet
townsfolk to believe that Dagan was just a nuisance child, and that he would
worsen and bring trouble in his adolescent years.

Just a week after his 6th birthday, Darius promised his son that he would take
him on his next voyage as a gift. The two ventured far south to the kingdom of
Chimeron to sell their crafted equipment to the nobility there. The journey
there was long and would be sure to take a physical and mental toll on a child
so young. However, not Dagan. He thrived on the thrill of traveling and the
sights he saw along the way. When they finally arrived, young Dagan was stunned
from what he saw. Unbeknownst to Dagan, he would be doing more than meeting with
the peerage of Chimeron, but also stand amongst the sea of people and witness
for himself the crowning of Chimeron's new king, King Pyr. As the landsmen of
Chimeron celebrated at the coronation, The boy could only stand in awe. In that
moment, Dagan promised himself that the day would come that he would seek his
own adventure and live a life of great importance. After six years, Dagan, now
just 12 years old, has witnessed his father succumb to disease. After the
passing of his father, Dagan began working all around Coal Town, and Victory,
the capital of Tuath Fasach, to provide for his mother. Lynessa could not stand
the guilt of being the reason as to why her son never left home anymore and
sacrificing as much as he has at such a young age.

In secret, Lynessa sent word to one Darius's avid customers and old friend, Sir
Radstar of Folkestone. Lynessa wanted Radstar to bring Dagan, south of Tuath
Fasach, to Folkestone and train him in the arts of combat. At first Dagan was
hesitant to leave, but his mother convinced him it was for a better life, one he
deserved.

It was in the year 1009 that Dagan was sworn into Folkestone and began training
with their elite. In the three years Dagan spent in Folkestone, he fought
numerous wars with them, in addition to defeating otherworldly foes. However, in
1012, Dagan had felt as if he learned all he could from Folkestone, and decided
he was ready for him to move on and find a place on his own in which he feels
like he belongs. With a heart warming goodbye, Dagan leaves Folkestone with his
gratitude, and a band of lifelong friends.

Dagan then made the decision to travel south for land he could start his life
over in. He thought back to one of his old sparring partners, Kahlenar
Swingline, a Sworn Sword of Ashenmark who with his company of other Warriors of
Ashenmark, has fought along side Folkestone on many occasions. After
reconnecting with Kahlenar and lengthy consideration, Dagan decided to return to
Ashenmark with Kahlenar. Dagan was quick to befriend all in Ashenmark and those
affiliated. It would seem Dagan had finally found what he had been searching for
all along, a place of belonging, and the adventures getting there. With Sworn
Sword and Dagan's mentor, KaelKatar, by his side, Dagan would officially be
sworn into Ashenmark in June of 1012 being granted the rank of initiate.

:::