---
title: Temorse Sorrowind
...

:::{.links}

* ## The High Seat
* [Temorse - Highlord](./temorse.html)
* [Kaelkatar - Knight Commander](./kaelkatar.html)
* [Shader - Champion](./shader.html)
* ## House of Lords
* [Twenaria](./twenaria.html)
* [Grindin](./grindin.html)
* [Killian](./killian.html)
* [Osric](./osric.html)
* [Umbra](./umbra.html)
* [Raynor](./raynor.html)
* [Malaki](./malaki.html)
* ## Sworn Swords
* [Kwido](./kwido.html)
* [Caleal](./caleal.html)
* [Luke-Jean](./luke-jean.html)
* [Tark](./tark.html)
* [Stripes](./stripes.html)
* [Moriah](./moriah.html)
* [Lyanna](./lyanna.html)
* ## Initiates
* ## Vassals
* ## Historic Members
* [Kahlenar](./former-members.html#kahlenar-swingline)
* [Keela](./former-members.html#keela-loveguard)
* [Dagan](./former-members.html#dagan-stormrider)

:::

:::{.content}

# Temorse Sorrowind

## Accolades and Titles

* Highlord of Ashenmark
* Knight of the Realms
* Knight of the Eternal Flame
* Blademaster
* Founding Member of the Order of the List
* Lord-Regent of Leneasa

## Biography

Temorse was born and raised in the Barony of Banecroft. When he came of age he
join the Baronial Militia where he trained for several years before joining up
with the Blue Falcons. It was at a gathering near Tom Rhyme's Tower where he
first encountered Autumn Rose, and in exchange for the aid in restoring a
friends soul, he pledged his service to the young goddess. Later he was lost in
Fae on a quest to save Bouquet, which later became known as the opening of
Bedlam in these Realms. He was recovered by the Dutchess of Song, however his
time spent in Fairie resulted with a metamorphasis into a fae being.

At a gathering in winter he and some fellow Blue Falcons ventured into the
lands of Tom the Rimer. There a close friend, the magus Alexander Davion was
slain and his soul held in limbo. There Temorse agreed to enter a covenant with
Autumn Rose, one of the 'small gods' where he would learn her ways and live by
them in exchange for her aid in restoring his friends. Over the years Temorse
has had several temples to Autumn Rose constructed in Corsica, Achoria, and
Ashenmark. He championed Autumn Rose at the Feast of Min where the Dealer and
Pale Queen came to be.

He eventually became lord of the town of Nadina, the town that served as a base
of operations for the Blue Falcons. He took up a squireship with lady Twenaria
of Elemirre to the Knights of the Eternal Flame during the Hunters Moon
festival shortly after. Later that year he traveled as a soldier in the army of
the Realms invasion force into Illinar. There a marriage was arranged between
him and the daughter of one of the merchant lords of the Jewel of Illinar,
Leneasa.

In 1010 he laid down his duties in Banecroft to found the nation of Ashenmark
with former Blue Falcons and Kahlenar Swingline, a gritty ploert formerly of
Ivory.

During the Bedlam incursion during the twentieth Feast of Chimeron Temorse was
slain within Bedlam, his soul presumably lost forever. However during the Black
Tide invasion of Bedlam of the Realms a bois resembling Temorse lead the
invasion of Ashenmark, where much of the unforested land was engulfed in Bedlam.

In the final stages of the Bedlam War the heroes of the Realms defeated the
Bois-Temorse during the Siege of Dark Hart, and eventually drove him through
Tale Unwritten, the the Northern Light blade gifted to Ashenmark by Pater Yule,
ending it and restoring Temorse to his original human form.

In 1020 at the Feast of the Leviathan, Temorse was honored with the mantle of
Knight of the Realms.

:::