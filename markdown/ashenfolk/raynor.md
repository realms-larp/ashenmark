---
title: Raynor Skyline
...

:::{.links}

* ## The High Seat
* [Temorse - Highlord](./temorse.html)
* [Kaelkatar - Knight Commander](./kaelkatar.html)
* [Shader - Champion](./shader.html)
* ## House of Lords
* [Twenaria](./twenaria.html)
* [Grindin](./grindin.html)
* [Killian](./killian.html)
* [Osric](./osric.html)
* [Umbra](./umbra.html)
* [Raynor](./raynor.html)
* [Malaki](./malaki.html)
* ## Sworn Swords
* [Kwido](./kwido.html)
* [Caleal](./caleal.html)
* [Luke-Jean](./luke-jean.html)
* [Tark](./tark.html)
* [Stripes](./stripes.html)
* [Moriah](./moriah.html)
* [Lyanna](./lyanna.html)
* ## Initiates
* ## Vassals
* ## Historic Members
* [Kahlenar](./former-members.html#kahlenar-swingline)
* [Keela](./former-members.html#keela-loveguard)
* [Dagan](./former-members.html#dagan-stormrider)

:::

:::{.content}

# Raynor Skyline

## Accolades and Titles

* Squire to the Knights of the Eternal Flame
* Squire to the Knights of Garm
* Squire to the Knights of the Sable Dragon

## Biography

Raynor was born in a future version of the Realms. He has no recollection of
where his birthplace was, but he recalls that the world was dark, covered in
the eternal darkness known as Bedlam. He spent most of his younger years hidden
indoors with his mother while his father fought against the forces of Bedlam.
Eventually, the powers grew too great and became unstoppable. The greatest
living Magi of the Realms united and through the power of Chronos they were
able to create a portal to send Raynor's father back in time to stop Bedlam.
However, just before the portal's completion, his father died hold back the
doors against Bedlam. The portal opened, and Raynor's mother pushed her six
year old son through the portal, in the hopes of giving him a better life.
Having entered the Realms in the present, Raynor spent his younger years
scavenging through the woods and living off of the land. He eventually found
his way to Fort Oakenbrook, and lived there and trained under Lord Sir Aeston
of Rhiassa. After his 18th birthday, Raynor joined the Oaken Guard, and served
with them for nearly two years. During his service with the Oaken Guard, Raynor
met and befriended Kahlenar of Ashenmark and Caleal of the Oaken Guard. He
quickly became interested in the nation of Ashenmark, and was granted an
initiate membership by Lord Temorse in 1011.

:::