---
title: Twenaria
...

:::{.links}

* ## The High Seat
* [Temorse - Highlord](./temorse.html)
* [Kaelkatar - Knight Commander](./kaelkatar.html)
* [Shader - Champion](./shader.html)
* ## House of Lords
* [Twenaria](./twenaria.html)
* [Grindin](./grindin.html)
* [Killian](./killian.html)
* [Osric](./osric.html)
* [Umbra](./umbra.html)
* [Raynor](./raynor.html)
* [Malaki](./malaki.html)
* ## Sworn Swords
* [Kwido](./kwido.html)
* [Caleal](./caleal.html)
* [Luke-Jean](./luke-jean.html)
* [Tark](./tark.html)
* [Stripes](./stripes.html)
* [Moriah](./moriah.html)
* [Lyanna](./lyanna.html)
* ## Initiates
* ## Vassals
* ## Historic Members
* [Kahlenar](./former-members.html#kahlenar-swingline)
* [Keela](./former-members.html#keela-loveguard)
* [Dagan](./former-members.html#dagan-stormrider)

:::

:::{.content}

# Twenaria of Elemirre

## Accolades and Titles

* Lady of Hart’s Hollow
* Knight of the Eternal Flame
* Priestess of Gaia
* Archers Guild

## Biography

Twenaria started our as an archer in the Borderguard. There she served as a
combatant and healer, mastering the healing arts as well as the bow. After she
founded the small woodland nation of Elemirre where she started the traditional
harvest feast; Hunters Moon. Shortly after she was knighted a Knight of the
Eternal Flame. She took a young Temorse Sorrowind as a squire, and eventually
gifted her lands of Elemirre to her squire and his fledgling nation.

:::