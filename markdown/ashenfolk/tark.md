---
title: Tark Bloodragon
...

:::{.links}

* ## The High Seat
* [Temorse - Highlord](./temorse.html)
* [Kaelkatar - Knight Commander](./kaelkatar.html)
* [Shader - Champion](./shader.html)
* ## House of Lords
* [Twenaria](./twenaria.html)
* [Grindin](./grindin.html)
* [Killian](./killian.html)
* [Osric](./osric.html)
* [Umbra](./umbra.html)
* [Raynor](./raynor.html)
* [Malaki](./malaki.html)
* ## Sworn Swords
* [Kwido](./kwido.html)
* [Caleal](./caleal.html)
* [Luke-Jean](./luke-jean.html)
* [Tark](./tark.html)
* [Stripes](./stripes.html)
* [Moriah](./moriah.html)
* [Lyanna](./lyanna.html)
* ## Initiates
* ## Vassals
* ## Historic Members
* [Kahlenar](./former-members.html#kahlenar-swingline)
* [Keela](./former-members.html#keela-loveguard)
* [Dagan](./former-members.html#dagan-stormrider)

:::

:::{.content}

# Tark Bloodragon

## Accolades and Titles

* Sworn Sword of Ashenmark

## Biography

Tark does not know his last name. Upon entering the civilized world he found he
needed a name. Having once witnessed the flight of a dragon and been aw struck
by its power and grace he penned his name Bloodragon. For a time he even
worshipped the Dragons, though infact he had never seen another since his first
sighting. He believed he drew his power from them for a time. When he left the
Blue Falcon to quest for further knowledge of himself he found that it was not
Dragons but himself that gave him power. Upon returning to the Realms and
finding his fellows in the new nation of Ashenmark he rejoined them, without
the illusion of any dragon, but himself as the ruler of his life. He dedicated
himself to the cause of Ashenmark and the practice of the sword.

After his time with the Blue Falcon, shortly after his run as champion for Team
Adara in the Tournaments of QoH, Tark became plagued by thoughts of doubt and
dark dreams that he was not where he should be. He set out to try and quell
these thoughts and settle his mind by seeing more of what the world could
offer. He spent two years wandering the world, he met knew people and saw
strange lands, but none of this eased his mind. He returned to the Realms
briefly 2 years later to find much had changed. His old nation, the Blue
Falcon, had disbanded. Many of its members had moved into new lands and formed
the nation of Ashenmark. Impressed by all this Tark sought to rejoin his
fellows and was greatly honored to be allowed initiation into the nation, he
would need to prove himself to them again after his absense before he was fully
accepted into their ranks. But first he had to prove something to himself. 
gain he left on a journey, again to try and overcome the dark thoughts he had,
to prove to himself that he was strong. It nearly killed him, and he returned
to the Realms 2 years later emaciated and weakened, but he had found a new
inner strength. Soon he regained his health and took up his new focus, the
sword. He dumped all he was into combat and training.

The sword became Tark's life. His only goals in life were to better himself
with a blade and support his friends in any way he could. Tournaments became
his drive, questing only to support his friends and train further for combat.
Tark continues look to improve his combat in any way possible. This has taken
him away from the realms a few times and continues to lead him on journeys to
other lands. Often he returns weak of body, but stronger of mind and and spirit
with new techniques and further drive, always looking to prove himself further
in the art of the sword.

The only thing to rivel his passion for combat, the nation of Ashenmark and his
comrades at arms. Spirited by the progress and name that his fellows had made
in the new nation of Ashenmark Tark devoted his swords to the nation. Taking
pride in the nations colors and wearing them with great dignity he wishes only
to further the nations name along with his prowess in combat. His time away and
dedication to the sword has kept him from devoting everything to the nation but
Tark has no stronger bond than with his friends in Ashenmark.

:::