---
title: Grindin Starbrook
...

:::{.links}

* ## The High Seat
* [Temorse - Highlord](./temorse.html)
* [Kaelkatar - Knight Commander](./kaelkatar.html)
* [Shader - Champion](./shader.html)
* ## House of Lords
* [Twenaria](./twenaria.html)
* [Grindin](./grindin.html)
* [Killian](./killian.html)
* [Osric](./osric.html)
* [Umbra](./umbra.html)
* [Raynor](./raynor.html)
* [Malaki](./malaki.html)
* ## Sworn Swords
* [Kwido](./kwido.html)
* [Caleal](./caleal.html)
* [Luke-Jean](./luke-jean.html)
* [Tark](./tark.html)
* [Stripes](./stripes.html)
* [Moriah](./moriah.html)
* [Lyanna](./lyanna.html)
* ## Initiates
* ## Vassals
* ## Historic Members
* [Kahlenar](./former-members.html#kahlenar-swingline)
* [Keela](./former-members.html#keela-loveguard)
* [Dagan](./former-members.html#dagan-stormrider)

:::

:::{.content}

# Grindin Starbrook

## Accolades and Titles

* Knight of the Sable Dragon
* Knight of Ashenmark
* Lord of Highmarket
* Apprentice to the Magi of the Realms

## Biography

Grindin hails from a town far in the north, by the name of Etiatil. In the
language of that land, it simply means "home." The townsfolk were simple,
enjoying a simple subsistence style of living, mostly farming the harsh soil.
Grindin was the youngest of the two sons of Gwidron Starbrook. Garric, the older
son is Grindin's half-brother, by a different mother. Gwidron had a taste for
elves, and both his wives descended from that fair race. Gwidron had spent much
of his life traveling, and had amassed a great deal of knowledge. Being older,
Garric had been born on the road, and had seen much of what life had to offer.
Grindin, on the other hand, had been born and raised in Etiatil, tending much
more to the ways of humans, than elves.

Garric was much more in tune with nature, tending towards magics of the forest,
spending long hours ranging in the woods. Grindin would spend more time
practicing skills considered useful to humans: simple survival, farming,
languages of commerce, healing. Despite all this, the Starbrook family never
truly fit in to the fabric of Etiatil. They were too... different. After a
particularly difficult year agriculturally, the townsfolk of Etiatil began to
resent and even blame the Starbrook family for their misfortune. Despite their
knowledge they could not come up with a way to coax the plants from the soil,
prevent the cattle from dying or bring the dead back to life.

Those ill feelings lead to the disaster one night in winter, where a number of
drunken peasants began rabble rousing, which culminated with an attack on the
Starbrook house, a small simple building full of books of learning, trinkets of
their travels, and tools of healing. Amidst the attack the building was set on
fire. In the final act of their lives, Gwidron and his wife bundled Grindin and
Garric in wet blankets and hurled them through a window into the night. The two
brothers escaped, their last memories of their parents were their howls of agony
as the hungry flames sucked their life from their bones. Garric, much more
attuned to the emotional being of elves has forever been haunted by their
screams, and once the brothers had reached physical safety, slowly descended
into insanity, constantly gibbering about fire, about screams in the night and
the fickleness of humans. Summoning all his healing prowess, Grindin was able to
restore his brother's mind, but both of them lost their magical abilities.

After wandering for many weeks, surviving off the land, and stealing when they
had to, Grindin and Garric came to be in the Barony of Bancroft, where they were
taken in by Baron Diamond. It was there that they, along with other younger
adventurers founded the Blue Falcon, a home and shelter for those who needed it.
Within a short time the Baron squired Grindin, who remained his squire for many
years, until he was deemed ready and knighted at Uncle Cecil's Yuletide
celebration. Eventually the Blue Falcon fell apart, as the adventurers matured
they found new homes throughout the Realms, and Grindin was part of the forming
of another nation: Ashenmark, where he resides today.

:::