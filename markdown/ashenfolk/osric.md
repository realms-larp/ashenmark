---
title: Osric Turnstone
...

:::{.links}

* ## The High Seat
* [Temorse - Highlord](./temorse.html)
* [Kaelkatar - Knight Commander](./kaelkatar.html)
* [Shader - Champion](./shader.html)
* ## House of Lords
* [Twenaria](./twenaria.html)
* [Grindin](./grindin.html)
* [Killian](./killian.html)
* [Osric](./osric.html)
* [Umbra](./umbra.html)
* [Raynor](./raynor.html)
* [Malaki](./malaki.html)
* ## Sworn Swords
* [Kwido](./kwido.html)
* [Caleal](./caleal.html)
* [Luke-Jean](./luke-jean.html)
* [Tark](./tark.html)
* [Stripes](./stripes.html)
* [Moriah](./moriah.html)
* [Lyanna](./lyanna.html)
* ## Initiates
* ## Vassals
* ## Historic Members
* [Kahlenar](./former-members.html#kahlenar-swingline)
* [Keela](./former-members.html#keela-loveguard)
* [Dagan](./former-members.html#dagan-stormrider)

:::

:::{.content}

# Osric Turnstone 

## Accolades and Titles

* Lord of Turnstone

## Biography

Osric hails from the ancient avian city of Ornithalia. These noble bird people
survived eons in their secret city high in the mountaintops. Frequently they
would fly out together and forage in parties. On one such outing that Osric
participated in he wandered off from the group and became lost. Having no sense
of timing or direction Osric was hopelessly stranded. However, as luck would
have it, he happened upon a cat person, normally a predator of his people, who
offered to help him find his way home. Osric revealed the location of his home
to the cat, who promply abandoned him. Weeks later Osric managed to find his
way back to Ornithalia, but to his dismay it was in ruin. Thousands had died in
a cat ambush and the few that remained were not happy to see him. He was
banished and cursed to take human form for his remaining years.

At North South War 1011 and 1012 Osric won the champion of Ashenmark tournament.

:::