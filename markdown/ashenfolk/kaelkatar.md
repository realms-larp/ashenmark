---
title: Kaelkatar Anderlys
...

:::{.links}

* ## The High Seat
* [Temorse - Highlord](./temorse.html)
* [Kaelkatar - Knight Commander](./kaelkatar.html)
* [Shader - Champion](./shader.html)
* ## House of Lords
* [Twenaria](./twenaria.html)
* [Grindin](./grindin.html)
* [Killian](./killian.html)
* [Osric](./osric.html)
* [Umbra](./umbra.html)
* [Raynor](./raynor.html)
* [Malaki](./malaki.html)
* ## Sworn Swords
* [Kwido](./kwido.html)
* [Caleal](./caleal.html)
* [Luke-Jean](./luke-jean.html)
* [Tark](./tark.html)
* [Stripes](./stripes.html)
* [Moriah](./moriah.html)
* [Lyanna](./lyanna.html)
* ## Initiates
* ## Vassals
* ## Historic Members
* [Kahlenar](./former-members.html#kahlenar-swingline)
* [Keela](./former-members.html#keela-loveguard)
* [Dagan](./former-members.html#dagan-stormrider)

:::

:::{.content}

# Kaelkatar Anderlys

## Accolades and Titles

* Magi of the Realms
* Lord of Goldenhall

## Biography

Kaelkatar Anderlys was born in a fishing village far in the northeast. As a
youth he was plagued with dark dreams always ending in his death and was exiled
as a pariah at a fairly young age. Traveling south he barely got by as a thief
and a trapper preferring solitude. Eventually he met an elderly apothecary who
cured him of his dreams. He then travelled to Nadina and joined an youthful
adventuring group, the Blue Falcons, under Baron Diamond of Banecroft.

Here he learned to fight with sword and shield and adventured across the land.
After a few years his dreams finally returned, but not with the darkness of his
youth. Instead the visions assumed the form of valiant knights, wise lords,
and---importantly---powerful healers. It was through meditation and
introspection of this dreams that Kael learned himself to heal wounds. He
continued on with the Falcons for a short time later, but inevitably decided to
part ways.

For years he followed a path of introspection and mastery of his craft. He
spent days training with sword and spell, and nights reliving the exploits of
the grandest of paladins. He again met the elderly apothecary who revealed to
him that the dreams and visions, both dark and heroic, were not simply dreams,
but past lives. Going deeper and deeper into the locked memories of his past
lives, Kael was able to learn that long ago he was a powerful sorcerer who
gained the favor of the gods and then abused. For this he was cursed to life
over and over again, tormented for his failures forever.

Most lives had been worthless existences, though some had overcome the curse to
find strength, power, and knowledge. He, determined to surpass all the lives
that had come before, headed back to the south. Learning that former
adventurers from Nadina had cleared lands in southern Elemirre and forged the
new nation Ashenmark, he traveled there and pledged his loyalty to their cause.

Recently his dreams and meditation have focused on the very first life he lived
as a sorcerer, many thousand years before countless planes away from here. He
has since devoted himself to the art of spellcraft in hopes that the power that
it brings will both aid in revealing his past and in forging his future and the
future of Ashenmark.

:::