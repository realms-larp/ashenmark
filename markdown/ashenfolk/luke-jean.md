---
title: Luke-Jean Havarti
...

:::{.links}

* ## The High Seat
* [Temorse - Highlord](./temorse.html)
* [Kaelkatar - Knight Commander](./kaelkatar.html)
* [Shader - Champion](./shader.html)
* ## House of Lords
* [Twenaria](./twenaria.html)
* [Grindin](./grindin.html)
* [Killian](./killian.html)
* [Osric](./osric.html)
* [Umbra](./umbra.html)
* [Raynor](./raynor.html)
* [Malaki](./malaki.html)
* ## Sworn Swords
* [Kwido](./kwido.html)
* [Caleal](./caleal.html)
* [Luke-Jean](./luke-jean.html)
* [Tark](./tark.html)
* [Stripes](./stripes.html)
* [Moriah](./moriah.html)
* [Lyanna](./lyanna.html)
* ## Initiates
* ## Vassals
* ## Historic Members
* [Kahlenar](./former-members.html#kahlenar-swingline)
* [Keela](./former-members.html#keela-loveguard)
* [Dagan](./former-members.html#dagan-stormrider)

:::

:::{.content}

# Luke-Jean Havarti

## Accolades and Titles

* Squire to ?

## Biography

Luke-John comes from a semi-wealthy merchant family. Dealing in decadent
cheeses among other sorts of products (hence the name). Each sibling in the
family grew up tending to the store, and when they became old enough they were
given their own branch elsewhere. However even as a child Luke-John never liked
the store all that much; it brought in money and he was fine with that aspect.
It was the day to day grind of selling useless cheeses and wares to people who
didn't need them which what bothered Luke-John so much. His interests lay in
combat and fighting. As a child he saw men and women wielding swords, sparring
with one another; and it fascinated him to no end. But soon enough the day came
for Luke-John to run his own shop as a part of the family business, a day he
dreaded. So for awhile he ran a store, and while it did prosper respectively
enough in that time, each day was worse than the last for Luke-John. Finally
enough was enough and it was time to leave. Armed with a sword and the clothes
on his back, Luke-John stumbled his way into Vinehaeven, right into fort
Oakenbrook. Recognizing his skill and ability, he was inducted into the
Oakenguard soon enough. However he still realizes he still has much to grow in
the way of combat; a fact that enthralls him to keep training and improving his
skills. Learning from masters of combat such as Sir Aeston, Sir Nighthawk, and
others he hopes to one day be as skilled as they are. In 1014 he and his fellow
Oakenguard members Shader, Moriah, and Lyanna were inducted into Ashenmark at
Black and White.

:::