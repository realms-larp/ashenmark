---
title: Killian Battlefate
...

:::{.links}

* ## The High Seat
* [Temorse - Highlord](./temorse.html)
* [Kaelkatar - Knight Commander](./kaelkatar.html)
* [Shader - Champion](./shader.html)
* ## House of Lords
* [Twenaria](./twenaria.html)
* [Grindin](./grindin.html)
* [Killian](./killian.html)
* [Osric](./osric.html)
* [Umbra](./umbra.html)
* [Raynor](./raynor.html)
* [Malaki](./malaki.html)
* ## Sworn Swords
* [Kwido](./kwido.html)
* [Caleal](./caleal.html)
* [Luke-Jean](./luke-jean.html)
* [Tark](./tark.html)
* [Stripes](./stripes.html)
* [Moriah](./moriah.html)
* [Lyanna](./lyanna.html)
* ## Initiates
* ## Vassals
* ## Historic Members
* [Kahlenar](./former-members.html#kahlenar-swingline)
* [Keela](./former-members.html#keela-loveguard)
* [Dagan](./former-members.html#dagan-stormrider)

:::

:::{.content}

# Killian Battlefate

## Accolades and Titles

* Knight Commander of the Knights of Garm
* Knight of Ashenmark
* Lord of New Battlefate Keep

## Biography

Killian doesn't know where he hails from. Hes only memories are few and faded
after he was found by the Blue Falcons wandering the lands of Banecroft. Within
the group he was not trusted, an unnamed wanderer with no memories. It was not
until he met Tark Bloodragon, who gave Killian his name, that he found his
place within the group. When the Blue Falcons disbanded he was one of very few
who still adventured in the Realms. In 1010 he became one of the founding
members of Ashenmark and in 1012 he ran against his cousin XT as a champion in
the Queen of Hearts Tournaments. Shortly after he was squired to the Knights of
Garm.

:::