---
title: Community
...

:::{.links}

* [Community](#community)
* [Other Realms Nations and Groups](#other-realms-nations-and-groups)
* [Social Media Links](#social-media-links)

:::

:::{.content}

# Community

Ashenmark is a nation in the New England based LARP (Live Action Role Play) The
Realms.  

The Realms is a world of medieval fantasy. Modern-day technology does not exist.
Instead, people live by wits and courage, by magic and the sword.

The Realms is a world far different from our own. It isn't based on our world's
history, and it's only loosely based on our own geography. Within the Realms can
be found such things as terrifying monsters, magical powers, armored knights,
and pious monks. The only limit to the contents of this new world is the limit
of the creative mind.

The Realms is a world where you can become a valiant fighter, a sneaky thief, or
a powerful sorcerer. You can become a ruler of men, a soldier of fortune, or a
peasant farmer.

Enter the Realms and learn the ways of the sword; become an apprentice mage or
squire to a knight; grab a goblet, join the feast, and listen to the bard's tale
as you gather at the banquet; compete in the tourney and improve your
swordsmanship; take up your bow and fire at the approaching army.

<https://www.realmsnet.net/>

## Other Realms Nations and Groups

Here is a brief list of other Realms nations and groups you may encounter at
events:

* [Rhiassa](http://www.rhiassa.com/)
* [Chimeron](https://chimeron.realmsnet.net/index.html)
* [Voraniss](https://voraniss.wordpress.com/)
* [Blackwood](https://nationofblackwood.com/)
* [Southern Wastes](https://southernwastes.com/toc.htm)
* [Library of Ivory](https://library.cityofivory.org/)

## Social Media Links

Here are links to some Realms-affiliated social media groups:

* [Realms Official Facebook](https://www.facebook.com/groups/therealmslarp)
* [Realms Discord Server]( https://discord.gg/jDFWUA4rbV)
* [Ashenmark Discord Server](https://discord.gg/nCxSjgHKS2)

:::