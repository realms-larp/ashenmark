---
title: Ashen Bounty
...

:::{.links}

* [Ashen Bounty](./ashen-bounty.html)
* [Ashen Adventures](./ashen-adventures.html)
* [Off the Hook](./off-the-hook.html)

:::

:::{.content}

Ashen Bounty is an annual event celebrating the harvest season with a feast as well as other activities. Traditionally the event lasts for three days and in the past has had a large focus on questing and feasting. There have been many changes to the event over the years, largely around how much the event has focused on the feast versus the questing, however we have come to land on a focus on the feast with questing relegated to the end of the event.

Currently what one can expect from the annual gathering that is Ashen Bounty is a casual first night with games and gambling being accompanied by mocktails and appetizers, and if weather allows, a campfire later into the night. There are often unique visitors that may show up on the first night, whether it being a small god or an important figure in current events. We have also moved our review competition to Friday for those adventurous eaters who would like to try some non-traditional snacks made from monstrous ingredients.

Saturday sees the feast in full, with a multicourse meal starting around noon and culminating in a main course around dinner time. During the day there are tournaments available for those interested in winning some gold for the auction later in the day. Those more adventurous can try out the Stoneheart Labyrinth, a complex maze that is changed each year to test the mettle of even the most seasoned adventurers. Depending on who is in attendance, a kids quest may take place as well. Once the sun begins to set, guests tend to find their way into the tavern where the main course is served followed by Ashenmark court. The night ends with the a gold auction and the serving of dessert before the more structured content ends. Campfires, gambling, and general revelry make up the remainder of the night.

Sunday will consist of a short quest, usually in line with whatever has happened at the most recent Ashen Adventurers. Guests are generally asked to prepare to have their gear out of cabins earlier so that the quest can go on longer into the afternoon before the event ends.

Overall, Ashen Bounty has become a multifaceted event with a wide variety of things to do, but also a time to relax and take things slow for those who want to.

:::