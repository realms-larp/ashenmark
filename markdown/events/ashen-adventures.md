---
title: Ashen Adventures
...

:::{.links}

* [Ashen Bounty](./ashen-bounty.html)
* [Ashen Adventures](./ashen-adventures.html)
* [Off the Hook](./off-the-hook.html)

:::

:::{.content}

Ashen Adventures is a fairly new annual event, started in 1024, that replaced the historical one day dungeon crawl event in the spring. In many ways Ashen Adventurers is the successor to Ashen Bounty’s of the past, focusing primarily on a heavy two to three days of questing. From Friday to Sunday the major content is questing content, and all the forms in which that might manifest. It is the flagship event if you want to stay up to date on the stories that the Ashenmark Questing Team tell.

While the focus is on questing, we try to make sure we do provide some amenities. On Friday night you can expect light refreshments, generally a small soup course as well, in terms of food. Both Saturday and Sunday we strive to provide a small pancake breakfast for those not wanting to leave the site for food. Refreshments are also available throughout the questing day Saturday and Sunday and a basic dinner is provided for those who wish to stay on site Saturday night.

Generally questing content will end around 7pm on Saturday, choosing a late dinner and time to reflect on the events of the quest instead of a traditional night quest on Saturday night. Although, those who are a big fan of night quests can still expect to get their fix on Friday night in some way shape or form.

In all questing content our team strives to create both stories and encounters that are engaging for players who are willing to interact with the content. We fully believe that LARP events shouldn’t just be participating in stories we tell, as they are a place for players' actions to have consequences, whether they be good or bad.

:::