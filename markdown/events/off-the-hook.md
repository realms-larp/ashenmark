---
title: Off the Hook
...

:::{.links}

* [Ashen Bounty](./ashen-bounty.html)
* [Ashen Adventures](./ashen-adventures.html)
* [Off the Hook](./off-the-hook.html)

:::

:::{.content}

Off the Hook is not an annual event, and in many ways is an extremely different experience than other Ashenmark events. The first, and currently only, Off the Hook event was intended as an irreverent, non-canon, fever dream of an event. The event is filled with anachronism, pop culture references, and satire. It is an event that our team views very much so separate from a normal event, but a time to laugh and embrace the ridiculousness aspect of LARPing and living in the modern world.

While we plan to throw another in the near future, the event is a massive undertaking, requiring a large amount of unique prop making, filming, and writing compared to a normal event. All In-House items released at Off the Hook cannot be made into magic item through the normal means, and we generally ask folks not to attempt to pass them at other events, as the items were not designed with any sort of game balance or sanity in mind, but instead to be fun to play with in the moment and become a nice memento of the event.

:::